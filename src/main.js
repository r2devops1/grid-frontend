import Vue from 'vue'
import App from './App.vue'
import Buefy from 'buefy'
import 'buefy/dist/buefy.css'
import axios from 'axios';

Vue.use(Buefy)

new Vue({
  el: '#app',
  render: h => h(App)
})
