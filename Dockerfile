FROM node:latest AS builder

LABEL maintainer="thomasboni13@gmail.com"

COPY src ./src
COPY webpack.config.js package.json package-lock.json ./
RUN npm install && \
    npm run build

FROM nginx:mainline

COPY docker-entrypoint.sh ./
COPY index.html /usr/share/nginx/html
COPY --from=builder dist /usr/share/nginx/html/dist

RUN chmod a+x ./docker-entrypoint.sh

ENTRYPOINT ["./docker-entrypoint.sh"]
CMD ["nginx", "-g", "daemon off;"]
