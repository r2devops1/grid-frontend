variable "DATABASE_PASSWORD" {
  description = "Databse password"
  type        = string
}

variable "FLASK_SECRET" {
  description = "Flask secret"
  type        = string
}

variable "KUBECONFIG" {
  description = "Path to a file containing kubeconfig"
  type        = string
}

variable "KUBE_NAMESPACE" {
  description = "The kube namespace to deploy kratos"
  type        = string
}

variable "GITLAB_REGISTRY_URL" {
  description = "URL to the r2devops/jobs Gitlab project's registry"
  type        = string
}

variable "GITLAB_REGISTRY_IMAGE_TAG" {
  description = "Tag of image to deploy"
  type        = string
}
